package hello;

import hello.aspect.Timed;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class HelloController implements HelloApi {

    public GreetingMessage index() {
        return new GreetingMessage("Greetings from Spring Boot!", "application");
    }

    @Timed
    public String test() throws Exception {
        //  imitate handling with 0-500 milliseconds
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(500));
        throw new Exception("any error message");
    }

    @ApiModel
    public class GreetingMessage {
        @ApiModelProperty(
                value = "String parameter",
                dataType = "String",
                example = "Greetings from Spring Boot!")
        String message;
        @ApiModelProperty(
                value = "Producer of greeting, always application",
                dataType = "String",
                example = "application"
        )
        String producer;

        public GreetingMessage(String message, String producer) {
            this.message = message;
            this.producer = producer;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getProducer() {
            return producer;
        }

        public void setProducer(String producer) {
            this.producer = producer;
        }
    }
}
