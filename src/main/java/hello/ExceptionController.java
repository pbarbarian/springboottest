package hello;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> exception(final Exception e) {
        return new ResponseEntity<>(new ErrorMessage(e.getLocalizedMessage(), "code.any.error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ApiModel
    static class ErrorMessage {
        @ApiModelProperty(
                value = "Provides description what is going wrong",
                dataType = "String",
                example = "The program has performed an illegal operation and will be shut down"
        )
        private String message;
        private String code;

        ErrorMessage(String message, String code) {
            this.message = message;
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
