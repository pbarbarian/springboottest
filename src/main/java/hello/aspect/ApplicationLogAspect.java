package hello.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @see <a href="http://niels.nu/blog/2017/spring-boot-aop.html">
 * Aspect Oriented Programming with Spring Boot</a>
 */

@Aspect
@Component
public class ApplicationLogAspect {

    @Around("@annotation(hello.aspect.Timed) && execution(public * *(..))")
    public Object timeLog(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long start = System.currentTimeMillis();

        Object value;

        try {
            value = proceedingJoinPoint.proceed();
        } finally {
            long duration = System.currentTimeMillis() - start;

            System.out.println(String.format(
                    "{%s}.{%s} took {%d} ms",
                    proceedingJoinPoint.getSignature().getDeclaringType().getSimpleName(),
                    proceedingJoinPoint.getSignature().getName(),
                    duration));
        }

        return value;
    }

    //    @Around("@annotation(org.springframework.web.bind.annotation.GetMapping) && execution(public * *(..))")
    @Around("execution(public * hello.HelloController.*())")
    public Object requestMethodLog(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .currentRequestAttributes())
                .getRequest();
        Object value;

        try {
            value = proceedingJoinPoint.proceed();
        } finally {
            System.out.println(String.format("[%-8s] {%-50s} {%s} from {%s}",
                    request.getMethod(),
                    request.getRequestURI(),
                    request.getRemoteAddr(),
                    request.getHeader("user-id")));
        }
        return value;
    }
}
