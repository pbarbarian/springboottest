package hello;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(
        value = "TEST endpoints",
        produces = "String",
        consumes = "",
        tags = {"hello", "text"})
@RestController
public interface HelloApi {

    @GetMapping("/")
    @ApiOperation(
            value = "display greetings",
            httpMethod = "GET")
    HelloController.GreetingMessage index();

    @GetMapping("/test")
    String test() throws Exception;
}
